# ETeMoX

**ETeMoX** is an architecture based in Temporal Models and Complex Event Processing to enable History-aware Explanations in *goal-oriented systems*. 

![](images/etemox.png)

The core components of the architecture are:
- Translator
- Filter
- Temporal Model
- Explainer

To test the implementation, a user requires: i) an RL system that exposes its decision-making traces, ii) a parser to translate these traces into the [trace-metamodel](https://dl.acm.org/doi/abs/10.1145/3419804.3420276), iii) a set of event patterns that define the filtering criteria (and their deployment to the Esper CEP engine), iv) a Hawk instance indexing the translated traces into a temporal model, and v) a set of temporal queries that extract the history-aware explanations from the temporal model.

## Architecture requirements

- [Java 11](https://www.java.com/en/download/manual.jsp).
- [Eclipse Modeling Tools](https://www.eclipse.org/downloads/packages/release/2022-03/r/eclipse-modeling-tools), in a recent release.
- [Eclipse Hawk](https://www.eclipse.org/hawk/) as the model indexing tool.
- Complex Event Processing Engine [Esper](https://www.espertech.com/) (Esper 8.3 provided). 
- MQTT message broker [Mosquito](https://mosquitto.org/) (If a local MQTT broker is used).
- MQTT client library [Eclipse Paho](https://www.eclipse.org/paho/)
- Dependency manager [Ivy](https://ant.apache.org/ivy/) 

## Installation

ETeMoX can be used from the [Eclipse IDE](https://www.eclipse.org/downloads/), or through its [Thrift API](https://www.eclipse.org/hawk/server/api/) (if using the [server](#etemox-server)).
The Thrift API makes it possible to use ETeMoX from other programs besides Eclipse: the Hawk website includes [examples](https://www.eclipse.org/hawk/server/other-languages/) for C++, JavaScript and Python.

### Eclipse IDE

1. Install [Hawk](https://www.eclipse.org/hawk/) according to the instructions on the website.
1. Download the [latest zipped ETeMoX update site](https://gitlab.com/sea-aston/etemox/-/packages/12534672) from GitLab.
1. From Eclipse, select "Help - Install New Software...", click on "Add", and use the "Archive" button to select the downloaded ZIP file.
1. Select all features and click on "OK".
1. Allow Eclipse to restart itself, and see the usage instructions below.

### ETeMoX server

1. Download and unzip the [latest server product](https://gitlab.com/sea-aston/etemox/-/packages/12537448) from GitLab that matches your operating system.
1. Launch the server:
    - If you are using Linux, run the `run-server.sh` script inside the main folder.
	- Otherwise, run the `hawk-server` executable inside the same folder.
1. After some time, you should see a `Welcome to the Hawk Server!` message: this means that the server is now waiting for connections.
1. To safely shut down the server, type `shutdown`, press Enter, then type `shutdown` and press Enter a second time.

## Case study instructions

Before starting, if you need a local MQTT broker, install it following the guidelines at the [Mosquitto website](https://mosquitto.org/download/).

Additionally, if you plan to query the index from outside Eclipse (e.g. from Python) through its Thrift API, you should [start the ETeMoX server](#etemox-server).

1. Download the sources of the [CEP project](https://gitlab.com/sea-aston/etemox/-/archive/main/etemox-main.zip?path=CEP/uk.ac.aston.cep.drones.json), and import it into your Eclipse instance.
1. From Eclipse, create a new Hawk instance "InstanceName" by following the [guidelines in the Hawk website](https://www.eclipse.org/hawk/basic-use/core-concepts/):
	- InstanceType:
	  - If you plan to query from inside Eclipse, use "Time-aware local Hawk" .
	  - If you plan to query from outside Eclipse (e.g. from Python), use "Time-aware Thrift-based remote Hawk".
	- Updater: Default Hawk Time Aware Model Updater
	- Min/Max Delay: 0 and 0
![](images/hawkInstance.png)
	- (Advanced) Metamodel parsers: EMF Metamodel Resource Factory
	- (Advanced) Model parsers: Drone resource factory 
		> Note: This will vary depending on the case study.
		
	- (Advanced) Query engines: EOL Query Engine and Time Aware EOL query Engine.
![](images/advancedHawk.png)
1. Configure the indexer ("InstanceName"):
	- Metamodels: stormlog.xcore
	![](images/metamodelDefi.png)
	- (Indexed Locations) Type: MQTT Connector 
	- (Indexed Locations) Location: MQTT topics to monitor (e.g., tcp://localhost:1883/initial_setting.json?extra=Q_table_collection_10.json)
 		> Note: This will vary depending on the broker and the MQTT port used, `localhost:1883` for a mosquitto local broker running on 1883 port.
    
    ![](images/addRepoMqtt.png)
1. Right-click on the `uk.ac.aston.cep.drones.json` project in the Project Explorer or Package Explorer views in Eclipse, and select "Run As - Java Application". This will initiate the CEP engine with the Filter criteria of Sampling every 10 steps.
1. Download the [sources of the Q-Learning case study](https://gitlab.com/sea-aston/etemox/-/archive/main/etemox-main.zip?path=Q-Learning) and check its [`README.md`](https://gitlab.com/sea-aston/etemox/-/blob/main/Q-Learning/README.md) for documentation. From a terminal, run:
```bash
python main.py
```
1. Download the [queries project](https://gitlab.com/sea-aston/etemox/-/archive/main/etemox-main.zip?path=hawk/EOLQueries) and import it into your Eclipse instance (`File -> Import -> Projects from Folder or Archive -> Directory -> ../path/to/etemoxFolder/hawk/EOLQueries ->  Select All`).
1. Test any of the temporal queries. Check the [Hawk documentation](https://www.eclipse.org/hawk/advanced-use/temporal-queries/#some-examples) for examples.
 ![](images/queryResult.png)

## Running from sources

1. Clone the present repository in the command line.
```bash
git clone [repo]
```
1. Open Eclipse IDE.
1. Import the Eclipse projects into the repository.
 `File -> Import -> Projects from Folder or Archive -> Directory -> ../path/to/etemoxFolder ->  Select All`
1. Install IvyDE 2.5.0. The update site URL for IvyDE can be found [here](https://ant.apache.org/ivy/ivyde/download.html).
1. Install Xcore, by using "Help - Install New Software...", selecting the update site for your Eclipse release, and searching for "Xcore".
1. Open the target platform definition in `uk.ac.aston.rdm.targetplaform`, let it resolve, and set it as your target platform.
1. Retrieve external libraries for `uk.ac.aston.hawk.mqtt` and `uk.ac.aston.hawk.mongodb` through IvyDE: right click on each project in the Package Explorer, and select "Ivy - Retrieve 'dependencies'".
1. Right-click on the `uk.ac.aston.logmodel`  project in the Package Explorer, and select "Run As - Eclipse Application". This corresponds to the Temporal Model component of our architecture.
1. To run the server from sources, go to the Project Explorer or Package Explorer, expand the `uk.ac.aston.etemox.server.product` project, right-click on the `Run ETeMoX Server.launch` file, and select `Run As - Run ETeMoX Server`.