package org.hawk.mongodb.drones;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommit;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;
import org.hawk.mongodb.AbstractMongoConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

/**
 * Exposes the drones MongoDB database as a Hawk repository. The connector uses
 * the episode + iteration numbers in the database to reshape it into a
 * timeline.
 *
 * The timeline starts with {@link #FIRST_REVISION}, which adds the
 * {@link #INITIAL_SETTINGS_DOCUMENT}. The following revisions will consist of
 * the various simulation steps, identified as <code>EPISODE_STEP</code>:
 * <ul>
 * <li>the first step of any episode will add its Q-table documents (which will
 * be named as {@link #Q_TABLE_DOCUMENT} + episode + "_" + drone +
 * ".json"),</li>
 * <li>and any other step will update those Q-table documents.</li>
 * </ul>
 *
 * A dedicated model parser component will need to be used for these files as
 * well.
 */
public class DroneMongoDatabase extends AbstractMongoConnector {

	private static final String DOCFIELD_DRONE = "drone number";
	private static final String DOCFIELD_STEP = "step";
	private static final String DOCFIELD_EPISODE = "episode";

	private Bson currentVersionFilter = new Document();

	private static final class MongoDocsIterable implements Iterable<VcsCommit> {
		private final StepInfo startStep;
		private final FindIterable<Document> docsIterable;
		private final StepInfo endStep;

		private MongoDocsIterable(StepInfo startStep, StepInfo endStep, FindIterable<Document> docsIterable) {
			this.startStep = startStep;
			this.endStep = endStep;
			this.docsIterable = docsIterable;
		}

		@Override
		public Iterator<VcsCommit> iterator() {
			MongoCursor<Document> itDocs = docsIterable.iterator();

			Document firstDoc = null;
			while (itDocs.hasNext()) {
				final Document doc = itDocs.next();
				final Optional<StepInfo> oStepInfo = getStepInfo(doc);
				if (!oStepInfo.isPresent()) {
					throw new IllegalStateException("Could not find step info in " + doc);
				}
				StepInfo stepInfo = oStepInfo.get();

				if (stepInfo.compareTo(startStep) >= 0) {
					// end of the skip range
					firstDoc = doc;
					break;
				}
			}

			if (firstDoc == null) {
				return Collections.emptyIterator();
			} else {
				return new MongoCommitsIterator(endStep, itDocs, firstDoc);
			}
		}
	}

	private static final class MongoCommitsIterator implements Iterator<VcsCommit> {
		private final StepInfo endStep;
		private final MongoCursor<Document> itDocs;

		private Document nextDoc;
		private boolean stop = false;

		private MongoCommitsIterator(StepInfo endStep, MongoCursor<Document> itDocs, Document firstDoc) {
			this.endStep = endStep;
			this.itDocs = itDocs;
			this.nextDoc = firstDoc;
		}

		@Override
		public boolean hasNext() {
			if (nextDoc != null) {
				return true;
			} else if (stop || !itDocs.hasNext()) {
				return false;
			} else {
				nextDoc = itDocs.next();

				final Optional<StepInfo> oStepInfo = getStepInfo(nextDoc);
				if (!oStepInfo.isPresent()) {
					throw new IllegalStateException("Could not find step info in " + nextDoc);
				}
				StepInfo stepInfo = oStepInfo.get();

				if (stepInfo.compareTo(endStep) > 0) {
					stop = true;
					return false;
				} else {
					return true;
				}
			}
		}

		@Override
		public VcsCommit next() {
			final Document doc = nextDoc;
			nextDoc = null;

			final Optional<StepInfo> oStepInfo = getStepInfo(doc);
			if (!oStepInfo.isPresent()) {
				throw new IllegalStateException("Could not find step info in " + doc);
			}
			StepInfo stepInfo = oStepInfo.get();

			VcsCommit commit = new VcsCommit();
			commit.setMessage("Changes for " + stepInfo.revision());
			commit.setJavaDate(new Date());
			commit.setRevision(stepInfo.revision());
			commit.setAuthor("no author recorded");

			Optional<Integer> droneNumber = getDroneNumber(doc);
			if (!droneNumber.isPresent()) {
				throw new IllegalStateException("Could not find drone number in " + doc); 
			}
			VcsCommitItem item = new VcsCommitItem();
			item.setChangeType(stepInfo.step == 0 ? VcsChangeType.ADDED : VcsChangeType.UPDATED);
			item.setCommit(commit);
			item.setPath(stepFilePath(stepInfo.episode, droneNumber.get()));
			commit.getItems().add(item);

			return commit;
		}
	}

	private static class StepInfo implements Comparable<StepInfo> {
		public final int step, episode;
		public final ObjectId docID;

		/**
		 * Creates a partial instance from an episode and a step. This instance is only
		 * useful as one of the limits for a certain range, for the sake of comparisons
		 * with {@link #hashCode()}.
		 */
		public StepInfo(int episode, int step) {
			this(episode, step, (ObjectId)null);
		}

		/**
		 * Creates a new instance from a document.
		 */
		public StepInfo(int episode, int step, Document doc) {
			this(episode, step, doc.getObjectId("_id"));
		}

		/**
		 * Creates a new instance from an identifier.
		 */
		public StepInfo(int episode, int step, ObjectId docID) {
			this.episode = episode;
			this.step = step;
			this.docID = docID;
		}

		public StepInfo(String revision) {
			if (!revision.contains("_")) {
				throw new IllegalArgumentException(revision + " is not a valid revision");
			}

			String parts[] = revision.split("_");
			this.episode = Integer.parseInt(parts[0]);
			this.step = Integer.parseInt(parts[1]);
			this.docID = new ObjectId(parts[2]);
		}

		public String revision() {
			return episode + "_" + step + "_" + docID;
		}

		@Override
		public int hashCode() {
			return Objects.hash(docID, episode, step);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StepInfo other = (StepInfo) obj;
			return Objects.equals(docID, other.docID) && episode == other.episode && step == other.step;
		}

		@Override
		public int compareTo(StepInfo o) {
			final int cmpEpisode = this.episode - o.episode;
			if (cmpEpisode == 0) {
				// ObjectID is not considered in the comparison
				final int cmpStep = this.step - o.step;
				if (cmpStep == 0) {
					if (docID == null) return -1;
					else if (o.docID == null) return 1;
					else return docID.compareTo(o.docID);
				} else {
					return cmpStep;
				}
			} else {
				return cmpEpisode;
			}
		}
	}

	static final String FIRST_REVISION = "0";
	static final String INITIAL_SETTINGS_COLLECTION = "initial_setting";
	public static final String INITIAL_SETTINGS_DOCUMENT = INITIAL_SETTINGS_COLLECTION + ".json";
	public static final String Q_TABLE_COLLECTION = "Q_table_collection";
	
	private static final Pattern RE_QTABLE_PATH = Pattern.compile(
		"/" + Q_TABLE_COLLECTION + "_([0-9]+)_([0-9]+).json"
	);

	private static final Logger LOGGER = LoggerFactory.getLogger(DroneMongoDatabase.class);

	@Override
	public String getCurrentRevision() throws Exception {
		/*
		 * TODO if the documents had episode: INT instead of using "episode: 0" as key,
		 * we would have a much easier job at sorting and filtering them. It's not a
		 * good idea to have different keys to represent these things.
		 */
		final FindIterable<Document> lastInsertedQTable = mongoDatabase
				.getCollection(Q_TABLE_COLLECTION)
				.find(currentVersionFilter)
				.sort(new Document("_id", -1)).limit(1);

		for (Document episodeDoc : lastInsertedQTable) {
			Optional<StepInfo> oInfo = getStepInfo(episodeDoc);
			if (oInfo.isPresent()) {
				return oInfo.get().revision();
			}
		}

		return FIRST_REVISION;
	}

	@Override
	public String getFirstRevision() throws Exception {
		return FIRST_REVISION;
	}

	@Override
	public Collection<VcsCommitItem> getDelta(String startRevision) throws Exception {
		if (startRevision.startsWith("-")) {
			startRevision = FIRST_REVISION;
		}
		VcsRepositoryDelta delta = getDelta(startRevision, getCurrentRevision());
		return delta.getCompactedCommitItems();
	}

	private Iterable<VcsCommit> firstVersionIterable(String startRevision) {
		return () -> {
			if (FIRST_REVISION.equals(startRevision) || startRevision == null || startRevision.startsWith("-")) {
				if (collectionExists(INITIAL_SETTINGS_COLLECTION)) {
					VcsCommit commit = new VcsCommit();
					commit.setMessage("Add initial settings");
					commit.setJavaDate(new Date());
					commit.setRevision(FIRST_REVISION);
					commit.setAuthor("no author recorded");

					final VcsCommitItem addSettingsItem = new VcsCommitItem();
					addSettingsItem.setChangeType(VcsChangeType.ADDED);
					addSettingsItem.setCommit(commit);
					addSettingsItem.setPath("/" + INITIAL_SETTINGS_DOCUMENT);
					commit.getItems().add(addSettingsItem);

					return Collections.singleton(commit).iterator();
				}
			}

			return Collections.emptyIterator();
		};
	}

	private Iterable<VcsCommit> docsIterable(String startRevision, String endRevision) throws Exception {
		if (!endRevision.contains("_")) {
			// Range does not go into the drone updates: end here
			return Collections::emptyIterator;
		}

		final StepInfo startStep = startRevision != null && startRevision.contains("_")
			? new StepInfo(startRevision) : new StepInfo(0, 0);
		final StepInfo endStep = endRevision.contains("_")
			? new StepInfo(endRevision) : new StepInfo(Integer.MAX_VALUE, Integer.MAX_VALUE);

		final FindIterable<Document> docsIterable = mongoDatabase
			.getCollection(Q_TABLE_COLLECTION)
			.find(rangeQuery(startStep, endStep))
			.sort(new Document("_id", 1));

		return new MongoDocsIterable(startStep, endStep, docsIterable);
	}

	private Bson rangeQuery(StepInfo startStep, StepInfo endStep) {
		return Filters.or(
			// episode = startEpisode && step >= startStep
			Filters.and(
				Filters.eq(DOCFIELD_EPISODE, startStep.episode),
				Filters.gte(DOCFIELD_STEP, startStep.step)),
			// episode > startEpisode && episode < endEpisode
			Filters.and(
				Filters.gt(DOCFIELD_EPISODE, startStep.episode),
				Filters.lt(DOCFIELD_EPISODE, endStep.episode)),
			// episode = endEpisode && step <= endStep
			Filters.and(
				Filters.eq(DOCFIELD_EPISODE, endStep.episode),
				Filters.lte(DOCFIELD_STEP, endStep.step)
			)
		);
	}

	@Override
	public VcsRepositoryDelta getDelta(String startRevision, String endRevision) throws Exception {
		Iterable<VcsCommit> commits = Iterables.concat(
			firstVersionIterable(startRevision),
			docsIterable(startRevision, endRevision));

		VcsRepositoryDelta delta = new VcsRepositoryDelta(commits);
		delta.setManager(this);
		return delta;
	}

	static String stepFilePath(int episode, int droneNumber) {
		return String.format("/%s_%d_%d.json", Q_TABLE_COLLECTION, episode, droneNumber);
	}

	@Override
	public File importFile(String revision, String path, File optionalTemp) {
		if (("/" + INITIAL_SETTINGS_DOCUMENT).equals(path)) {
			FindIterable<Document> iniSettings = mongoDatabase
				.getCollection(INITIAL_SETTINGS_COLLECTION).find().limit(1);
			return saveDocumentToFile(iniSettings, optionalTemp);
		}

		/*
		 * FIXME: path is not really considered here, only the objectId in the revision.
		 *
		 * This is done because we can't build an index to quickly find a document by
		 * episode + step + drone combination, due to the current Mongo database
		 * structure. The documents would need to have those fields, and a compound
		 * index would be needed.
		 */

		Matcher pathMatcher = RE_QTABLE_PATH.matcher(path);
		if (pathMatcher.matches()&& !FIRST_REVISION.equals(revision)) {
			final StepInfo stepInfo = new StepInfo(revision);
			final FindIterable<Document> qTable = mongoDatabase
				.getCollection(Q_TABLE_COLLECTION)
				.find(new Document("_id", stepInfo.docID)).limit(1);
			return saveDocumentToFile(qTable, optionalTemp);
		}

		LOGGER.error("Unknown path: {} or revision: {}", path,revision);
		return null;
	}

	private File saveDocumentToFile(FindIterable<Document> itDocument, File fDestination) {
		for (Document doc : itDocument) {
			try (FileWriter fW = new FileWriter(fDestination)) {
				fW.write(doc.toJson());
				return fDestination;
			} catch (IOException e) {
				LOGGER.error("Could not save document to " + fDestination, e);
			}
		}
		return null;
	}

	@Override
	public String getRepositoryPath(String rawPath) {
		return rawPath;
	}

	@Override
	public String getHumanReadableName() {
		return "Mongo Drones Database";
	}

	/**
	 * Returns the current filter on the episodes and steps we want to index. This
	 * is useful when we only want to index some of the episodes.
	 */
	public Bson getCurrentVersionFilter() {
		return currentVersionFilter;
	}

	/**
	 * Changes the filter applied on the set of episodes and steps we want to index.
	 */
	public void setCurrentVersionFilter(Bson currentVersionFilter) {
		this.currentVersionFilter = currentVersionFilter;
	}

	private static Optional<StepInfo> getStepInfo(Document episodeDoc) {
		Integer episode = episodeDoc.getInteger(DOCFIELD_EPISODE);
		Integer step = episodeDoc.getInteger(DOCFIELD_STEP);
		if (episode != null && step != null) {
			return Optional.of(new StepInfo(episode, step, episodeDoc));
		}
		return Optional.empty();
	}

	private static Optional<Integer> getDroneNumber(Document doc) {
		Integer drone = doc.getInteger(DOCFIELD_DRONE);
		if (drone != null) {
			return Optional.of(drone);
		}
		return Optional.empty();
	}

	private boolean collectionExists(String name) {
		for (String cname : mongoDatabase.listCollectionNames()) {
			if (cname.equals(name)) {
				return true;
			}
		}
		return false;
	}
}
