/**
 */
package uk.ac.aston.stormlog;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.BooleanMeasurement#isValue <em>Value</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getBooleanMeasurement()
 * @model
 * @generated
 */
public interface BooleanMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(boolean)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getBooleanMeasurement_Value()
	 * @model unique="false"
	 * @generated
	 */
	boolean isValue();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.BooleanMeasurement#isValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isValue()
	 * @generated
	 */
	void setValue(boolean value);

} // BooleanMeasurement
