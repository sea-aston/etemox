/**
 */
package uk.ac.aston.stormlog.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.NFRSatisfaction;
import uk.ac.aston.stormlog.RewardTableRow;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reward Table Row</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableRowImpl#getSatisfactions <em>Satisfactions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableRowImpl#getAction <em>Action</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableRowImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RewardTableRowImpl extends MinimalEObjectImpl.Container implements RewardTableRow {
	/**
	 * The cached value of the '{@link #getSatisfactions() <em>Satisfactions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatisfactions()
	 * @generated
	 * @ordered
	 */
	protected EList<NFRSatisfaction> satisfactions;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RewardTableRowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.REWARD_TABLE_ROW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NFRSatisfaction> getSatisfactions() {
		if (satisfactions == null) {
			satisfactions = new EObjectContainmentEList<NFRSatisfaction>(NFRSatisfaction.class, this, StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS);
		}
		return satisfactions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action getAction() {
		if (action != null && action.eIsProxy()) {
			InternalEObject oldAction = (InternalEObject)action;
			action = (Action)eResolveProxy(oldAction);
			if (action != oldAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.REWARD_TABLE_ROW__ACTION, oldAction, action));
			}
		}
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAction(Action newAction) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.REWARD_TABLE_ROW__ACTION, oldAction, action));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Double> getValue() {
		if (value == null) {
			value = new EDataTypeEList<Double>(Double.class, this, StormlogPackage.REWARD_TABLE_ROW__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS:
				return ((InternalEList<?>)getSatisfactions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS:
				return getSatisfactions();
			case StormlogPackage.REWARD_TABLE_ROW__ACTION:
				if (resolve) return getAction();
				return basicGetAction();
			case StormlogPackage.REWARD_TABLE_ROW__VALUE:
				return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS:
				getSatisfactions().clear();
				getSatisfactions().addAll((Collection<? extends NFRSatisfaction>)newValue);
				return;
			case StormlogPackage.REWARD_TABLE_ROW__ACTION:
				setAction((Action)newValue);
				return;
			case StormlogPackage.REWARD_TABLE_ROW__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS:
				getSatisfactions().clear();
				return;
			case StormlogPackage.REWARD_TABLE_ROW__ACTION:
				setAction((Action)null);
				return;
			case StormlogPackage.REWARD_TABLE_ROW__VALUE:
				getValue().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE_ROW__SATISFACTIONS:
				return satisfactions != null && !satisfactions.isEmpty();
			case StormlogPackage.REWARD_TABLE_ROW__ACTION:
				return action != null;
			case StormlogPackage.REWARD_TABLE_ROW__VALUE:
				return value != null && !value.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //RewardTableRowImpl
