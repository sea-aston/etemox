/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Log</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.Log#getTimesliceID <em>Timeslice ID</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getDecisions <em>Decisions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getActions <em>Actions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getRequirements <em>Requirements</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getMeasures <em>Measures</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getObservations <em>Observations</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Log#getAgents <em>Agents</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getLog()
 * @model
 * @generated
 */
public interface Log extends EObject {
	/**
	 * Returns the value of the '<em><b>Timeslice ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeslice ID</em>' attribute.
	 * @see #setTimesliceID(String)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_TimesliceID()
	 * @model unique="false"
	 * @generated
	 */
	String getTimesliceID();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Log#getTimesliceID <em>Timeslice ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeslice ID</em>' attribute.
	 * @see #getTimesliceID()
	 * @generated
	 */
	void setTimesliceID(String value);

	/**
	 * Returns the value of the '<em><b>Decisions</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Decision}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decisions</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Decisions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Decision> getDecisions();

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Action}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getActions();

	/**
	 * Returns the value of the '<em><b>Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.NFR}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirements</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Requirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<NFR> getRequirements();

	/**
	 * Returns the value of the '<em><b>Measures</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Measure}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measures</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Measures()
	 * @model containment="true"
	 * @generated
	 */
	EList<Measure> getMeasures();

	/**
	 * Returns the value of the '<em><b>Observations</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Observation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observations</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Observations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Observation> getObservations();

	/**
	 * Returns the value of the '<em><b>Agents</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Agent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agents</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getLog_Agents()
	 * @model containment="true"
	 * @generated
	 */
	EList<Agent> getAgents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" measureNameUnique="false"
	 * @generated
	 */
	Measure getMeasure(String measureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" actionNameUnique="false"
	 * @generated
	 */
	Action getAction(String actionName);

} // Log
