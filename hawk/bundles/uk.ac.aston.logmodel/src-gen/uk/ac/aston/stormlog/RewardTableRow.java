/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reward Table Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.RewardTableRow#getSatisfactions <em>Satisfactions</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.RewardTableRow#getAction <em>Action</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.RewardTableRow#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableRow()
 * @model
 * @generated
 */
public interface RewardTableRow extends EObject {
	/**
	 * Returns the value of the '<em><b>Satisfactions</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.NFRSatisfaction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfactions</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableRow_Satisfactions()
	 * @model containment="true"
	 * @generated
	 */
	EList<NFRSatisfaction> getSatisfactions();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableRow_Action()
	 * @model
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.RewardTableRow#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTableRow_Value()
	 * @model unique="false"
	 * @generated
	 */
	EList<Double> getValue();

} // RewardTableRow
