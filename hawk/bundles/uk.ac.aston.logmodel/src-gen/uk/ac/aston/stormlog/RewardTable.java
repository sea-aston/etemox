/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reward Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.RewardTable#getRows <em>Rows</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.RewardTable#getThresholds <em>Thresholds</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTable()
 * @model
 * @generated
 */
public interface RewardTable extends EObject {
	/**
	 * Returns the value of the '<em><b>Rows</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.RewardTableRow}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rows</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTable_Rows()
	 * @model containment="true"
	 * @generated
	 */
	EList<RewardTableRow> getRows();

	/**
	 * Returns the value of the '<em><b>Thresholds</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.RewardTableThreshold}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thresholds</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getRewardTable_Thresholds()
	 * @model containment="true"
	 * @generated
	 */
	EList<RewardTableThreshold> getThresholds();

} // RewardTable
