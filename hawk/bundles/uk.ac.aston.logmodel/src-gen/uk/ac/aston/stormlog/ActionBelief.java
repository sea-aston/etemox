/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Belief</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.ActionBelief#getAction <em>Action</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.ActionBelief#getEstimatedValue <em>Estimated Value</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getActionBelief()
 * @model
 * @generated
 */
public interface ActionBelief extends EObject {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getActionBelief_Action()
	 * @model
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.ActionBelief#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Estimated Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Estimated Value</em>' attribute.
	 * @see #setEstimatedValue(double)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getActionBelief_EstimatedValue()
	 * @model unique="false"
	 * @generated
	 */
	double getEstimatedValue();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.ActionBelief#getEstimatedValue <em>Estimated Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Estimated Value</em>' attribute.
	 * @see #getEstimatedValue()
	 * @generated
	 */
	void setEstimatedValue(double value);

} // ActionBelief
