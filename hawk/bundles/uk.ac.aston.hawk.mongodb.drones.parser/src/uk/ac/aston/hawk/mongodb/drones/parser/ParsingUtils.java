package uk.ac.aston.hawk.mongodb.drones.parser;

import org.bson.Document;

import uk.ac.aston.stormlog.DoubleListMeasurement;

final class ParsingUtils {

	private ParsingUtils() {}

	public static void parseTuplesSubdocument(Document subdoc, DoubleListMeasurement m) {
		for (Object subvalue : subdoc.values()) {
			if (!(subvalue instanceof String)) {
				throw new IllegalArgumentException(
					"Nested value in a subdoc is not a string");
			}

			final String sTuple = (String)subvalue;
			if (!sTuple.startsWith("(") || !sTuple.endsWith(")")) {
				throw new IllegalArgumentException(String.format(
					"String '%s' does not follow the expected '(a, b, c)' format", sTuple));
			}

			final String noBrackets = sTuple.substring(1, sTuple.length() - 1);
			final String[] sNumbers = noBrackets.split("\\s*,\\s*");
			for (String sNumber : sNumbers) {
				sNumber = sNumber.trim();
				final double number = "-inf".equals(sNumber) ? Double.NEGATIVE_INFINITY : Double.parseDouble(sNumber);
				m.getValue().add(number);
			}
		}
	}
}
