package uk.ac.aston.hawk.mongodb.drones.parser;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.hawk.core.IFileImporter;
import org.eclipse.hawk.core.IModelResourceFactory;
import org.eclipse.hawk.core.model.IHawkModelResource;
import org.eclipse.hawk.emf.EMFWrapperFactory;
import org.eclipse.hawk.emf.model.EMFModelResource;
import org.hawk.mongodb.drones.DroneMongoDatabase;

import uk.ac.aston.stormlog.Log;

public class MongoDroneResourceFactory implements IModelResourceFactory {

	@Override
	public String getHumanReadableName() {
		return "Mongo Drone document resource factory";
	}

	@Override
	public IHawkModelResource parse(IFileImporter importer, File changedFile) throws Exception {
		if (changedFile.getName().equals(DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT)) {
			final InitialSettingsConverter initialConverter = new InitialSettingsConverter(changedFile);
			final Log initialSettingsLog = initialConverter.getLog();
			final Resource rIS = wrapAsResource(initialSettingsLog, changedFile);
			return new EMFModelResource(rIS, new EMFWrapperFactory(), this);
		} else {
			final File fInitial = importer.importFile("/" + DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT);
			final InitialSettingsConverter initialConverter = new InitialSettingsConverter(fInitial);
			final Log initialSettingsLog = initialConverter.getLog();
			wrapAsResource(initialSettingsLog, fInitial);

			final QTableConverter qTableConverter = new QTableConverter(initialSettingsLog, changedFile);
			final Log qTableLog = qTableConverter.getLog();
			final Resource rQ = wrapAsResource(qTableLog, changedFile);

			return new EMFModelResource(rQ, new EMFWrapperFactory(), this);
		}
	}

	private Resource wrapAsResource(final Log initialSettingsLog, File changedFile) {
		Resource rIS = new ResourceImpl();
		rIS.setURI(URI.createFileURI(changedFile.getAbsolutePath()));
		rIS.getContents().add(initialSettingsLog);
		return rIS;
	}

	@Override
	public void shutdown() {
		// nothing to do!
	}

	@Override
	public boolean canParse(File f) {
		final String fn = f.getName();
		if (DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT.equals(fn)) {
			return true;
		} else if (fn.endsWith(".json") && fn.contains(DroneMongoDatabase.Q_TABLE_COLLECTION)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Collection<String> getModelExtensions() {
		return Collections.singleton(".json");
	}

}
