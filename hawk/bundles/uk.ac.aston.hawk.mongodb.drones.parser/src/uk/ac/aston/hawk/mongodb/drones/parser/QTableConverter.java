package uk.ac.aston.hawk.mongodb.drones.parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map.Entry;

import org.bson.Document;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.ActionBelief;
import uk.ac.aston.stormlog.Agent;
import uk.ac.aston.stormlog.Decision;
import uk.ac.aston.stormlog.DoubleListMeasurement;
import uk.ac.aston.stormlog.IntegerMeasurement;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Measure;
import uk.ac.aston.stormlog.Observation;
import uk.ac.aston.stormlog.StormlogFactory;

/**
 * Converts the settings of a Q-table document to the STORM log metamodel. These settings
 * reference the measures and actions in the initial settings document, to avoid repetition
 * and make the links to the common concepts more clear.
 */
public class QTableConverter {

	private static final StormlogFactory FACTORY = StormlogFactory.eINSTANCE;

	private final Document doc;
	private final Log baseLog;
	private Log log;

	private Measure mSINR;
	private Measure mState;
	private Measure mStep;
	private Measure mDrone;
	private Measure mEpisode;
	private Measure mRewardTotal;
	private Measure mRewardDrone;

	public QTableConverter(Log baseLog, File f) throws IOException {
		this(baseLog, new String(Files.readAllBytes(f.toPath())));
	}

	/**
	 * Creates a converter for a Q-table document JSON string.
	 * 
	 * @param baseLog Log produced with the {@link InitialSettingsConverter} from
	 *                the initial settings document.
	 * @param json    String with the JSON source of the Q-table document.
	 */
	public QTableConverter(Log baseLog, String json) {
		this.doc = Document.parse(json);
		this.baseLog = baseLog;
	}

	public synchronized Log getLog() {
		if (log == null) {
			createLog();
		}
		return log;
	}

	private void createLog() {
		this.log = FACTORY.createLog();	
		loadMeasures();
		final int nEpisode = doc.getInteger("episode");
		final int nStep = doc.getInteger("step");
		final int nDrone = doc.getInteger("drone_number");
		final String agent = doc.getString("agent");
		System.out.println(nEpisode+"_"+nStep);
		System.out.println("agent: "+agent+" "+nDrone);
		log.setTimesliceID(nEpisode+"_"+nStep);
		addAgent(agent);
		addObservation(nEpisode,nStep,nDrone);
		addDecision();
		
	}

	private void addAgent(String agentName) {
		Agent agent = FACTORY.createAgent();
		agent.setName(agentName);
		log.getAgents().add(agent);
	}

	private void loadMeasures() {
		mEpisode = baseLog.getMeasure(InitialSettingsConverter.MEASURE_EPISODE);
		mStep = baseLog.getMeasure(InitialSettingsConverter.MEASURE_STEP);
		mDrone = baseLog.getMeasure(InitialSettingsConverter.MEASURE_DRONE);
		mRewardTotal = baseLog.getMeasure(InitialSettingsConverter.MEASURE_REWARD_TOTAL);
		mRewardDrone = baseLog.getMeasure(InitialSettingsConverter.MEASURE_REWARD_DRONE);
		mSINR = baseLog.getMeasure(InitialSettingsConverter.MEASURE_SINR);
		mState = baseLog.getMeasure(InitialSettingsConverter.MEASURE_DRONEPOS);
	}
	
	private void addObservation(int nEpisode, int nStep, int nDrone) {

		Observation obs = FACTORY.createObservation();
		//if drone_number is 1 or 0 save the following + drone_number {
		obs.setDescription(String.format("Observation for %s, %s", nEpisode, nStep));				
		//else agent name we will create a version or not ? 
		log.getObservations().add(obs);
		
		IntegerMeasurement mmEpisode = FACTORY.createIntegerMeasurement();
		mmEpisode.setMeasure(mEpisode);
		mmEpisode.setValue(nEpisode);
		obs.getMeasurements().add(mmEpisode);
		
		IntegerMeasurement mmStep = FACTORY.createIntegerMeasurement();
		mmStep.setMeasure(mStep);
		mmStep.setValue(nStep);
		obs.getMeasurements().add(mmStep);
		

		IntegerMeasurement mmDrone = FACTORY.createIntegerMeasurement();
		mmDrone.setMeasure(mDrone);
		mmDrone.setValue(nDrone);
		obs.getMeasurements().add(mmDrone);
		
		for (String qtableKey : doc.keySet()) {
			if (qtableKey.startsWith("qtable")) {
				Document qtableSubdoc = (Document)doc.get(qtableKey);
				
				Document sinrSubdoc = (Document) qtableSubdoc.get("SINR");
				DoubleListMeasurement mmSINR = FACTORY.createDoubleListMeasurement();
				mmSINR.setMeasure(mSINR);
				ParsingUtils.parseTuplesSubdocument(sinrSubdoc, mmSINR);
				obs.getMeasurements().add(mmSINR);

				Document stateSubdoc = (Document) qtableSubdoc.get("state");
				DoubleListMeasurement mmState = FACTORY.createDoubleListMeasurement();
				mmState.setMeasure(mState);
				ParsingUtils.parseTuplesSubdocument(stateSubdoc, mmState);
				obs.getMeasurements().add(mmState);
				
				Document rewardSubdoc =(Document) qtableSubdoc.get("reward");
							
				final int mmRewardTotal=rewardSubdoc.getInteger("total");
				//System.out.println(mmRewardTotal);
				IntegerMeasurement mrewardTotal = FACTORY.createIntegerMeasurement();
				mrewardTotal.setMeasure(mRewardTotal);
				mrewardTotal.setValue(mmRewardTotal);
				obs.getMeasurements().add(mrewardTotal);
				
				final int mmRewardDrone=rewardSubdoc.getInteger(Integer.toString(nDrone));
				IntegerMeasurement mrewardDrone = FACTORY.createIntegerMeasurement();
				mrewardDrone.setMeasure(mRewardDrone);
				mrewardDrone.setValue(mmRewardDrone);
				obs.getMeasurements().add(mrewardDrone);
				}
		}
		}

	private void addDecision() {

		final IntegerMeasurement mmDrone = (IntegerMeasurement) log.getObservations().get(0).getFirstMeasurement(mDrone);

		final Decision dec = FACTORY.createDecision();
		dec.setName("Decision Drone " + mmDrone.getValue());
		log.getDecisions().add(dec);
		
		final Document qtableSubdoc = (Document)doc.get("qtable");

		final Document stateSubdoc = (Document) qtableSubdoc.get("state");
		// JSON - Differences on DQN uses "drone" and for Qlearning uses "drone:"
		String[] sXY = null;
		String sDronePos= null;
		try {
			sDronePos = stateSubdoc.get("drone: " + mmDrone.getValue()).toString();
		}catch(Exception e) {
		}finally {
			if(sDronePos== null) {
				sDronePos = stateSubdoc.get("drone " + mmDrone.getValue()).toString();
			}
		}
		sXY = sDronePos.substring(1, sDronePos.length() - 1).split(",");

		// JSON - state.drone uses doubles for positions, but Q-table uses integers 
		final int droneX = (int) Double.parseDouble(sXY[0].trim());
		final int droneY = (int) Double.parseDouble(sXY[1].trim());
		final Document qTableRowSubdoc = (Document) qtableSubdoc.get(String.format("position: (%d, %d)", droneX, droneY));
		System.out.println(String.format("position: (%d, %d)", droneX, droneY));
		if (!qTableRowSubdoc.isEmpty()){
			for (Entry<String, Object> blfEntry : qTableRowSubdoc.entrySet()) {
				//System.out.println("blf entry>>> " + blfEntry.toString());
				final String blfKey = blfEntry.getKey();
				double blfValue = 0; 
				Object value = qTableRowSubdoc.get(blfKey);
				//JSON - Q table entry uses double values in Q-learning and string values in DQN   
				if (value instanceof Number) {
					final double dValue = ((Number)value).doubleValue();
					blfValue = dValue;
				}else if (value instanceof String) {
					final String stringBlfValue = value.toString();
					blfValue= Double.valueOf(stringBlfValue.substring(2, stringBlfValue.length()-1));
				}else if (value instanceof Float || value instanceof Double) {
					final double dValue = ((Number)value).doubleValue();
					blfValue = dValue;
				}else {
					throw new IllegalArgumentException(String.format(
						"Unknown value type %s (key %s)",
						value.getClass(), blfKey));
				}
			
			addActionBelief(dec, baseLog.getAction(translateAction(blfKey)), blfValue);
			}
		}
		final String sActionTaken = qtableSubdoc.getString("action");
		dec.setActionTaken(baseLog.getAction(translateAction(sActionTaken)));
	}
	
	protected void addActionBelief(final Decision dec, Action action, double qmatrixValue ) {
		ActionBelief belief = FACTORY.createActionBelief();
		belief.setAction(action);
		belief.setEstimatedValue(qmatrixValue);
		dec.getActionBeliefs().add(belief);
	}
	
	/**
	 * Maps the names of the actions in the new JSON files to the names of the
	 * actions we have used in our previous mappings.
	 */
	private String translateAction(String jsonName) {
		switch (jsonName) {
		case "west": return InitialSettingsConverter.ACTION_LEFT;
		case "east": return InitialSettingsConverter.ACTION_RIGHT;
		case "north": return InitialSettingsConverter.ACTION_UP;
		case "south": return InitialSettingsConverter.ACTION_DOWN;
		case "stay": return InitialSettingsConverter.ACTION_STAY;
		default: throw new IllegalArgumentException("Unknown JSON action " + jsonName);
		}
	}
}
